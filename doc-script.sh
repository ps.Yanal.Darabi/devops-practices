#!/bin/bash


LAST_COMMIT_DATE=$(git log -1 --pretty=format:%cd --date=format:%y%m%d)


COMMIT_HASH=$(git rev-parse --short=8 HEAD)


version="${LAST_COMMIT_DATE}-${COMMIT_HASH}"

export version

echo "$version"




