#!/bin/bash


LAST_COMMIT_DATE=$(git log -1 --pretty=format:%cd --date=format:%y%m%d)


COMMIT_HASH=$(git rev-parse --short=8 HEAD)


VERSION="${LAST_COMMIT_DATE}-${COMMIT_HASH}"


echo "$VERSION"


mv target/assignment-0.0.1-SNAPSHOT.jar target/${VERSION}.jar

