FROM openjdk:latest

COPY /target/240324-17ce11d5.jar /.


ENV SPRING_DATASOURCE_URL=jdbc:h2:mem:testdb \
    SPRING_DATASOURCE_DRIVER_CLASS_NAME=org.h2.Driver 
    



CMD ["java","-jar","240324-17ce11d5.jar"]



